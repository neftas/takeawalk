package com.relentlesscoding.takeawalk.view


import com.relentlesscoding.takeawalk.ShowWeatherReportHandler
import com.relentlesscoding.takeawalk.logic.ErrorHandler
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import static CommandLineView.DEFAULT_LOCATION

class CommandLineViewTest extends Specification {

    ShowWeatherReportHandler showWeatherReportHandler = Mock()
    ErrorHandler errorHandler = Stub()
    @Subject CommandLineView commandLineView = new CommandLineView(showWeatherReportHandler, errorHandler)

    @Unroll('parse(#args) should show weather for coming #daysCount days')
    def 'test number of days arguments'() {
        when:
        commandLineView.parse(args)

        then:
        1 * showWeatherReportHandler.showWeatherReport(DEFAULT_LOCATION, daysCount)

        where:
        args        | daysCount
        ['-1']      | 1
        ['--one']   | 1
        ['-2']      | 2
        ['--two']   | 2
        ['-3']      | 3
        ['--three'] | 3
        ['-4']      | 4
        ['--four']  | 4
        ['-5']      | 5
        ['--five']  | 5
        ['--week']  | 7
        ['-n 8']    | 8
        ['-100']    | 1
    }

    @Unroll('parse(#args) should show weather for location #expectedLocation')
    def 'test location arguments'() {
        when:
        commandLineView.parse(args)

        then:
        1 * showWeatherReportHandler.showWeatherReport(expectedLocation, 1)

        where:
        args                        | expectedLocation
        ['-l', 'Amsterdam']         | 'Amsterdam'
        ['--location', 'Amsterdam'] | 'Amsterdam'
        []                          | DEFAULT_LOCATION
    }
}
