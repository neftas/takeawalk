package com.relentlesscoding.takeawalk.logic

import groovy.json.JsonSlurper
import spock.lang.Specification
import spock.lang.Subject

class LocationFetcherSpec extends Specification {

    ErrorHandler errorHandler = Mock()
    @Subject LocationFetcher locationFetcher = new LocationFetcher(errorHandler, 'Utrecht')

    def setup() {
        def resource = getClass().classLoader.getResourceAsStream('locs_test.json')
        assert resource
        locationFetcher.response = new JsonSlurper().parse(resource)
    }

    def 'response should contain multiple locations'() {
        when:
        locationFetcher.parseResponse()

        then:
        locationFetcher.multipleResults
        locationFetcher.multipleResults.size() == 3
    }
}
