package com.relentlesscoding.takeawalk.logic


import groovy.json.JsonSlurper
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

class WeatherFetcherSpec extends Specification {

    ErrorHandler errorHandler = Mock()
    com.relentlesscoding.takeawalk.model.WeatherLocation weatherLocation = Stub()
    @Subject WeatherFetcher weatherFetcher = new WeatherFetcher(errorHandler, weatherLocation)

    def 'empty response should trigger error exit'() {
        given:
        weatherFetcher.response = null

        when:
        weatherFetcher.parseResponse()

        then:
        1 * errorHandler.exitWithError(*_)
        thrown NullPointerException  // because errorHandler mock won't System.exit
    }

    @Unroll
    def "WeatherDay with index #a should be more recent than index #b"() {
        given:
        weatherFetcher.response = new JsonSlurper().parse(getClass().classLoader.getResourceAsStream('output_test.json'))

        when:
        weatherFetcher.parseResponse()

        then:
        with(weatherFetcher.result) {
            weatherDays[a].date < weatherDays[b].date
        }

        where:
        a  | b
        0  | 13
        1  | 2
        3  | 13
        4  | 13
        12 | 13
    }
}
