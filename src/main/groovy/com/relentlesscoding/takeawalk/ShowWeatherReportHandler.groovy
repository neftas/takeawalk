package com.relentlesscoding.takeawalk

import com.relentlesscoding.takeawalk.logic.InfoFetcher
import com.relentlesscoding.takeawalk.logic.LocationFetcherFactory
import com.relentlesscoding.takeawalk.logic.WeatherFetcherFactory
import com.relentlesscoding.takeawalk.model.WeatherLocation
import com.relentlesscoding.takeawalk.model.WeatherReport

import javax.inject.Inject
import javax.inject.Singleton

/**
 * Controller
 */
@Singleton
class ShowWeatherReportHandler {

    final LocationFetcherFactory locationFetcherFactory
    final WeatherFetcherFactory weatherFetcherFactory

    @Inject
    ShowWeatherReportHandler(LocationFetcherFactory locationFetcherFactory, WeatherFetcherFactory weatherFetcherFactory) {
        this.locationFetcherFactory = locationFetcherFactory
        this.weatherFetcherFactory = weatherFetcherFactory
    }

    def showWeatherReport(String strLoc = '', Integer numOfDays = 1) {
        InfoFetcher<WeatherLocation> locationFetcher = locationFetcherFactory.create(strLoc)
        WeatherLocation location = locationFetcher.getInfo()

        InfoFetcher<WeatherReport> weatherReportFetcher = weatherFetcherFactory.create(location)
        def weatherReport = weatherReportFetcher.getInfo()

        weatherReport.prettyPrint(numOfDays)
    }

}
