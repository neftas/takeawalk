package com.relentlesscoding.takeawalk.view

import com.relentlesscoding.takeawalk.ShowWeatherReportHandler
import com.relentlesscoding.takeawalk.logic.ErrorHandler

import javax.inject.Inject
import javax.inject.Singleton

import static com.relentlesscoding.takeawalk.logic.ErrorHandler.ErrorCode.INVALID_ARGUMENT
import static com.relentlesscoding.takeawalk.logic.ErrorHandler.ErrorCode.OPTIONS_NULL

/**
 * The View of the application, presenting the command-line interface.
 */
@Singleton
class CommandLineView {

    public static final String DEFAULT_LOCATION = 'De Bilt'

    final CliBuilder cli
    final ShowWeatherReportHandler showWeatherReportHandler
    final ErrorHandler errorHandler

    @Inject
    CommandLineView(ShowWeatherReportHandler showWeatherReportHandler, ErrorHandler errorHandler) {
        this.showWeatherReportHandler = showWeatherReportHandler
        this.errorHandler = errorHandler

        cli = new CliBuilder(usage: 'takeawalk [options]', header: 'Options:')
        cli.with {
            '1' longOpt: 'one', 'Shows today'
            '2' longOpt: 'two', 'Shows today and tomorrow'
            '3' longOpt: 'three', 'Shows three days in a row'
            '4' longOpt: 'four', 'Shows four days'
            '5' longOpt: 'five', 'Shows five days'
            _ longOpt: 'week', 'Shows seven days'
            n args: 1, argName: 'day count', 'Shows n number of days'
            l longOpt: 'location', args: 1, argName: 'location', required: false, 'The location to be queried'
            h longOpt: 'help', 'Show this help text'
        }
    }

    void parse(args) {
        OptionAccessor options = cli.parse(args)

        if (!options) errorHandler.exitWithError('options is null', OPTIONS_NULL)

        if (options.help) {
            cli.usage()
            System.exit 0
        }

        String location = options.location ?: DEFAULT_LOCATION

        def showWeatherForAmountOfDays = showWeatherReportHandler.&showWeatherReport.curry(location)

        // code duplication: should find a way to deduplicate this
        // how can we find which option was actually set?
        if (options.n) {
            if (!options.n.isInteger()) {
                errorHandler.exitWithError("argument to -n is not an integer (was ${options.n})", INVALID_ARGUMENT)
            }
            showWeatherForAmountOfDays options.n as Integer
        } else if (options.'2') {
            showWeatherForAmountOfDays 2
        } else if (options.'3') {
            showWeatherForAmountOfDays 3
        } else if (options.'4') {
            showWeatherForAmountOfDays 4
        } else if (options.'5') {
            showWeatherForAmountOfDays 5
        } else if (options.week) {
            showWeatherForAmountOfDays 7
        } else {
            showWeatherForAmountOfDays 1
        }
    }
}
