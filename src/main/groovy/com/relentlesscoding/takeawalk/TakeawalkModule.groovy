package com.relentlesscoding.takeawalk

import com.google.inject.AbstractModule
import com.google.inject.assistedinject.FactoryModuleBuilder
import com.relentlesscoding.takeawalk.logic.LocationFetcher
import com.relentlesscoding.takeawalk.logic.LocationFetcherFactory
import com.relentlesscoding.takeawalk.logic.WeatherFetcher
import com.relentlesscoding.takeawalk.logic.WeatherFetcherFactory

class TakeawalkModule extends AbstractModule {
    @Override
    protected void configure() {
        install(new FactoryModuleBuilder()
                .implement(LocationFetcher, LocationFetcher)
                .build(LocationFetcherFactory))
        install(new FactoryModuleBuilder()
                .implement(WeatherFetcher, WeatherFetcher)
                .build(WeatherFetcherFactory))
    }
}
