package com.relentlesscoding.takeawalk

import com.google.inject.Guice
import com.google.inject.Injector
import com.relentlesscoding.takeawalk.view.CommandLineView

class Main {
    // setup program, nothing more
    static main(args) {
        Injector injector = Guice.createInjector(new TakeawalkModule())
        injector.getInstance(CommandLineView).parse(args)
    }
}
