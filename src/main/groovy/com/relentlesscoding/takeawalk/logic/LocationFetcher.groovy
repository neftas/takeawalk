package com.relentlesscoding.takeawalk.logic

import com.google.inject.assistedinject.Assisted
import com.relentlesscoding.takeawalk.model.UUIDGenerator
import com.relentlesscoding.takeawalk.model.WeatherLocation
import groovy.json.JsonException
import groovy.json.JsonSlurper

import javax.inject.Inject

import static com.relentlesscoding.takeawalk.logic.ErrorHandler.ErrorCode.CONSOLE_NOT_AVAILABLE
import static com.relentlesscoding.takeawalk.logic.ErrorHandler.ErrorCode.LOCATION_NOT_FOUND

class LocationFetcher extends InfoFetcher<WeatherLocation> {
    private ErrorHandler errorHandler
    private String location
    private exitWithLocationError
    private List<Object> multipleResults

    @Inject
    LocationFetcher(ErrorHandler errorHandler, @Assisted String location) {
        this.errorHandler = errorHandler
        this.location = location
        this.exitWithLocationError = errorHandler.&exitWithError.curry("Could not locate location '$location'.", LOCATION_NOT_FOUND)
        createUrl()
    }

    @Override
    void fetchResource() {
        try {
            response = new JsonSlurper().parse(url)
        } catch (JsonException jsonException) {
            System.err << "Could not reach Buienradar due to following error:\n${jsonException.getCause()}"
            jsonException.printStackTrace(System.err)
        }
    }

    @Override
    void parseResponse() {
        if (!response) exitWithLocationError()

        multipleResults = response
    }

    @Override
    void provideChoice() {
        int choice
        if (multipleResults.size() > 1) {
            printChoices()
            choice = getUserDecision()
        } else {
            choice = 1
        }

        Object jsonLoc = multipleResults[choice - 1]

        if (jsonLoc) {
            String id = jsonLoc.id
            String location = jsonLoc.name
            result = new WeatherLocation(location, id)
        } else exitWithLocationError()
    }

    void printChoices() {
        multipleResults.eachWithIndex { Object entry, int index ->
            println "${index + 1}. ${entry.name} (${entry.country})"
        }
    }

    private int getUserDecision() {
        if (System.console() == null) errorHandler.exitWithError('Console not available', CONSOLE_NOT_AVAILABLE)

        def choice = System.console().readLine("Please choose one [1 - ${multipleResults.size()}] (default = 1):")
        if (choice in ['', null]) choice = 1

        try {
            choice as int
        } catch (NumberFormatException ignored) {
            System.err.println "A valid integer should be entered"
            return getUserDecision()
        }

        choice = choice as int
        if (choice < 1 || choice > multipleResults.size()) {
            System.err.println "Number should be between 1 and ${multipleResults.size()}"
            return getUserDecision()
        }

        return choice
    }

    private void createUrl() {
        String encodedUserLoc = URLEncoder.encode(location, 'UTF-8')
        String uuid = UUIDGenerator.INSTANCE.createUUID()
        url = "https://location.buienradar.nl/1.1/location/search?query=$encodedUserLoc&ak=$uuid".toURL()
    }
}
