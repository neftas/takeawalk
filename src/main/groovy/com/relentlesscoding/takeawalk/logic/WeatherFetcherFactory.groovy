package com.relentlesscoding.takeawalk.logic

import com.relentlesscoding.takeawalk.model.WeatherLocation

interface WeatherFetcherFactory {
    WeatherFetcher create(WeatherLocation weatherLocation)
}