package com.relentlesscoding.takeawalk.logic

interface LocationFetcherFactory {
    LocationFetcher create(String location)
}