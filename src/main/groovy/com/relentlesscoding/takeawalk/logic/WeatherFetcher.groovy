package com.relentlesscoding.takeawalk.logic

import com.google.inject.assistedinject.Assisted
import com.relentlesscoding.takeawalk.model.WeatherDay
import com.relentlesscoding.takeawalk.model.WeatherHour
import com.relentlesscoding.takeawalk.model.WeatherLocation
import com.relentlesscoding.takeawalk.model.WeatherReport
import groovy.json.JsonException
import groovy.json.JsonSlurper

import javax.inject.Inject
import java.time.ZonedDateTime

import static com.relentlesscoding.takeawalk.logic.ErrorHandler.ErrorCode.RESPONSE_EMPTY

class WeatherFetcher extends InfoFetcher<WeatherReport> {
    final ErrorHandler errorHandler
    final WeatherLocation location
    final ZonedDateTime creationTime = ZonedDateTime.now()

    @Inject
    WeatherFetcher(ErrorHandler errorHandler, @Assisted WeatherLocation weatherLocation) {
        this.errorHandler = errorHandler
        this.location = weatherLocation
        createUrl()
    }

    @Override
    void fetchResource() {
        try {
            response = new JsonSlurper().parse(url)
        } catch (JsonException jsonException) {
            System.err << "Could not reach Buienradar due to following error:\n${jsonException.getCause()}"
            jsonException.printStackTrace(System.err)
        }
    }

    @Override
    void parseResponse() {
        if (response == null) errorHandler.exitWithError("Response for location $location was empty", RESPONSE_EMPTY)

        List<WeatherDay> weatherDays = response.days.collect { it as WeatherDay }
        weatherDays.each {
            it.hours = it.hours.collect { it as WeatherHour }
        }
        weatherDays.sort { oneDay, anotherDay -> oneDay.date <=> anotherDay.date }

        result = new WeatherReport(location, weatherDays)
    }

    void createUrl() {
        url = "https://forecast.buienradar.nl/2.0/forecast/${location.id}".toURL()
    }
}
