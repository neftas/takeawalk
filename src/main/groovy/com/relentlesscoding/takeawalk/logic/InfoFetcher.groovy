package com.relentlesscoding.takeawalk.logic

abstract class InfoFetcher<T> {
    URL url
    Object response
    T result

    final T getInfo() {
        fetchResource()
        parseResponse()
        provideChoice()
        return result
    }

    abstract void fetchResource()

    abstract void parseResponse()

    void provideChoice() {}
}
