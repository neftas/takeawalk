package com.relentlesscoding.takeawalk.logic

class ErrorHandler {
    enum ErrorCode {
        LOCATION_NOT_FOUND(3),
        INVALID_ARGUMENT(4),
        RESPONSE_EMPTY(5),
        CONSOLE_NOT_AVAILABLE(6),
        OPTIONS_NULL(127)

        int exitCode

        private ErrorCode(int exitCode) {
            this.exitCode = exitCode
        }
    }

    void exitWithError(String errorMsg, ErrorCode errorCode) {
        System.err.println(errorMsg)
        System.exit(errorCode.exitCode)
    }
}
