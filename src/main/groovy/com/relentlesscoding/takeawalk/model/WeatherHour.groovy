package com.relentlesscoding.takeawalk.model

import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode(includeFields = true)
class WeatherHour implements IgnoreUnknownProperties {
    String datetime
    BigDecimal hour
    BigDecimal temperature
    BigDecimal feeltemperature
    Integer windspeed
    Integer beaufort
    String winddirection
    Integer humidity
    Double precipitationmm
    Integer precipationmm
    Integer precipation
    Integer precipitation
    Integer sunshinepower
    Integer sunshine
    Integer sunpower

    Date getDatetime() {
        Utils.getDateFromRemoteString(datetime)
    }
}
