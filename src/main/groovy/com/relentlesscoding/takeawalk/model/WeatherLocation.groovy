package com.relentlesscoding.takeawalk.model

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import groovy.transform.TupleConstructor

@TupleConstructor
@ToString(includes = "location")
@EqualsAndHashCode(includes = ["location", "id"])
class WeatherLocation {
    String location
    String id
}
