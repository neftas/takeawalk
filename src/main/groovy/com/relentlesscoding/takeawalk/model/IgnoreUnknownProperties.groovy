package com.relentlesscoding.takeawalk.model

trait IgnoreUnknownProperties {
    def propertyMissing(String name, Object value) {}
}
