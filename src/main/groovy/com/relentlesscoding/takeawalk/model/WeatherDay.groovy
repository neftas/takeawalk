package com.relentlesscoding.takeawalk.model

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@ToString
@EqualsAndHashCode(includeFields = true, excludes = ["hours"])
class WeatherDay implements IgnoreUnknownProperties {
    String date
    String sunrise
    String sunset
    List<WeatherHour> hours
    BigDecimal maxtemp
    BigDecimal mintemp
    BigDecimal mintemperature
    BigDecimal temperature
    BigDecimal feeltemperature
    BigDecimal windspeed
    BigDecimal beaufort
    String winddirection
    BigDecimal precipicationmm
    BigDecimal precipitation

    Date getDate() {
        Utils.getDateFromRemoteString(date)
    }

    Date getSunrise() {
        Utils.getDateFromRemoteString(sunrise)
    }

    Date getSunset() {
        Utils.getDateFromRemoteString(sunset)
    }


}
