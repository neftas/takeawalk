package com.relentlesscoding.takeawalk.model

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import groovy.transform.TupleConstructor

@TupleConstructor
@ToString(includes = "weatherDays")
@EqualsAndHashCode(includes = ["location", "weatherDays"])
class WeatherReport {
    WeatherLocation location
    List<WeatherDay> weatherDays

    void prettyPrint(int numberOfDaysToPrint) {
        weatherDays.subList(0, numberOfDaysToPrint).eachWithIndex { WeatherDay weatherDay, int index ->
            println ">>> ${weatherDay.date.format('EEEE dd MMMM yyyy')} in ${location.location}"
            println String.format('%5s\t%5s\t%5s\t%4s\t%4s\t%8s\t%4s\t%4s',
                    'TIJD', 'TEMP', 'GTEMP', 'WIND', 'KANS', 'NSLAG', 'ZON', 'ZONK')
            weatherDay.hours.each { WeatherHour weatherHour ->
                StringBuilder weatherRowStringBuilder = new StringBuilder()
                weatherRowStringBuilder.with {
                    // time | temp | perception | wind direction | wind speed | precip chance | precip mm | sun chance | sun power
                    append String.format('%5s\t', "${weatherHour.datetime.format('HH:mm')}")
                    append String.format('%5.1f\t', weatherHour.temperature)
                    append String.format('%5.1f\t', weatherHour.feeltemperature)
                    append String.format('%2s ', weatherHour.winddirection)
                    append String.format('%1d\t', weatherHour.beaufort)
                    // TODO (neftas): add UTF-8 icons here indication wind direction
                    append String.format('%3d%%\t', weatherHour.precipitation)
                    if (weatherHour.precipitationmm == 0) {
                        append String.format('%6.0fmm\t', weatherHour.precipitationmm)
                    } else {
                        append String.format('%6.1fmm\t', weatherHour.precipitationmm)
                    }
                    append String.format('%3d%%\t', weatherHour.sunshine)
                    append String.format('%4d\t', weatherHour.sunshinepower)
                }
                println weatherRowStringBuilder
            }
            printEmptyLine(index, numberOfDaysToPrint)
        }
    }

    private boolean printEmptyLine(int index, int numberOfDaysToPrint) {
        if (index + 1 < numberOfDaysToPrint) println()
    }
}
