package com.relentlesscoding.takeawalk.model

import java.text.SimpleDateFormat

class Utils {

    static Date getDateFromRemoteString(String date) {
        new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(date)
    }
}
