package com.relentlesscoding.takeawalk.model

import java.security.SecureRandom

/**
 * Idea stolen from https://stackoverflow.com/questions/41107/how-to-generate-a-random-alpha-numeric-string#41156
 */
enum UUIDGenerator {
    INSTANCE

    SecureRandom random = new SecureRandom()

    String createUUID() {
        new BigInteger(130, random).toString(32)
    }
}
